"""M3U files writer module"""

from xml.etree.ElementTree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement



def print_xspf(file_name, elem_list):
    """Write elements to a XSPF file

    Keyword Arguments:
    file_name -- file name
    elem_list -- list of paths to write in the file
    """
    # <?xml version="1.0" encoding="UTF-8"?>
    playlist = Element("playlist")
    playlist.set("version", "1")
    playlist.set("xmlns", "http://xspf.org/ns/0/")

    track_list = SubElement(playlist, "trackList")

    for line in elem_list:
        track = SubElement(track_list, "track")
        SubElement(track, "location").text = line
    tree = ElementTree(playlist)
    tree.write(file_name, xml_declaration="""<?xml version="1.0" encoding="UTF-8"?>""")
