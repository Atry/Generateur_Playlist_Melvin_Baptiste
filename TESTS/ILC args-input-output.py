#!/usr/bin/env python3
import sys

def main():
    name = input("who are you? ")
    filename = sys.argv[0]
    print("welcome " + name + " in " + filename + "!\n")
    argc = 0
    for arg in sys.argv:
        print("arg #" + str(argc) + "\t " + arg + "\n")
        argc += 1

if __name__ == "__main__":
    main()
