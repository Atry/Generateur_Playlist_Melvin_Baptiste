#!/usr/bin/env python3
"""Main module of mblist"""

import random

from . import CLI
from . import access_data_base
from .logger import log
from .writer_m3u import print_m3u
from .writer_xspf import print_xspf
from .international import init_i18n
_ = init_i18n()


def mixlist(list_to_mix):
    """mix the list and return the mixed one"""
    not_mixed_list = list_to_mix[:]
    mixed_list = []
    while len(not_mixed_list) > 0:
        index = random.randrange(0, len(not_mixed_list))
        mixed_list.append(not_mixed_list.pop(index))
    return mixed_list


def output(playlist):
    """Output the final playlist to files/cli

    where to output is determined but arguments from CLI module

    Keyword Arguments:
    playlist -- list of paths to output
    """
    if len(CLI.args.M3U_file) > 0:
        log.info(_("outputing to M3U..."))
        print_m3u(CLI.args.M3U_file, playlist)
    if len(CLI.args.XSPF_file) > 0:
        log.info(_("outputing to XSPF..."))
        print_xspf(CLI.args.XSPF_file, playlist)
    if CLI.args.out_cli or (len(CLI.args.M3U_file) == 0
                            and len(CLI.args.M3U_file) == 0):
        log.info(_("outputing to console..."))
        for item in playlist:
            if CLI.args.colors:
                print(str("\x1B[1m\x1B[") 
                      + str(mixlist(["31", "32", "33", "34", "35", "36", "37", "1;31", "1;33", "1;35", "1;36"]).pop(0))
                      + str("m") + item)
            else:
                print(item)


def getmusiclist(column, expr, prop):
    """Returns a list of matching musics
    list will be clamped so total duration will be just more
    than args.duration*prop

    Keyword Arguments:
    column -- name of the column corresponding to a criteria
    expr -- content of the criteria, a regular expression
    prop -- proportion of the lengh of the music (float between 0.0 and 1.0)
    """
    log.info(_("creating music list of lenght > args.duration*prop"))
    log.debug("column==%s ; expr==%s ; prop==%s" % (str(column),
                                                    str(expr),
                                                    str(prop)))
    # get tuples (path,duration) from bdd
    matching_list = access_data_base.get_musics_path_and_duration(column, expr)
    # lol
    if CLI.args.years:
        anc_matching_list = matching_list[:]
        for item in anc_matching_list:
            print("duplicating " + str(item) + " ...")
            for i in range(0, 8760):
                matching_list.append(item)
    # mix the list
    mixed_list = mixlist(matching_list)
    # clamp list...
    clamped_list = []
    total_time = 0
    while len(mixed_list) > 0:
        next_time = total_time + int(mixed_list[0][1])
        if total_time < CLI.args.duration * prop:
            clamped_list.append(mixed_list.pop(0))
            total_time = next_time
        elif CLI.args.noExceedPerCriteriaProp:
            # ... without exceeding maximum ...
            break
        else:
            # ... exceeding maximum with 1 music
            clamped_list.append(mixed_list.pop(0))
            total_time = next_time
            break

    # we did it!
    log.info(_("created sub music list"))
    log.debug(_("count: %s ; duration(seconds): %s ; expected(seconds): %s")
              % (str(len(clamped_list)),
                 str(total_time),
                 str(CLI.args.duration * prop)))
    return clamped_list


def main():
    """The main method where everything is done with magic and candies"""
    # some asserts
    if CLI.args.duration < 1800:
        log.warning("duration < 30!")
        print(_("You should make playlists of more than half an hour \
for better results..."))

    # connect to database
    if not access_data_base.connect_to_data_base():
        log.critical(_("failed to connect to database, exiting!"))
        exit(1)

    # the list to fill with musics, used to fill the playlist
    musics = []

    # for each criteria complete the musics list
    total_prop_percent = 0
    correspondances_criteres_base = {"genre": "genre",
                                     "subgenre": "sousgenre",
                                     "artist": "artiste",
                                     "album": "album",
                                     "title": "titre"}
    total_duration = 0
    for critere in correspondances_criteres_base:
        for arg in getattr(CLI.args, critere):
            total_prop_percent += int(arg[1])
            for music in getmusiclist(correspondances_criteres_base[critere],
                                      arg[0],
                                      float(arg[1])/100.0):
                musics.append(music)
                total_duration += int(music[1])
    log.info(_("all queries done, total musics count: ") + str(len(musics)))
    log.debug("duration: " + str(total_duration))
    if total_prop_percent < 100:
        missing_prop = 100 - total_prop_percent
        log.warning(_("filling %s%% of the playlist with random musics")
                    % str(missing_prop))
        for music in getmusiclist("titre", ".*", missing_prop):
            musics.append(music)
    if total_prop_percent > 100:
        log.warning(_("prop was %s instead of 100!")
                    % (str(total_prop_percent)))

    # disconnect from database
    access_data_base.disconnect_from_data_base()

    # mix the final list
    musics = mixlist(musics)
    log.info(_("mixed musics"))

    # clamp musics list to create final playlist
    playlist = []  # playlist (contains only paths)
    total_duration = 0
    while len(musics) > 0:
        if total_duration + int(musics[0][1]) > CLI.args.duration:
            break  # over maximum duration, cancel this item and break out of the loop
        else:
            total_duration += int(musics[0][1])
            playlist.append(musics.pop(0)[0])
    print(_("playlist generated ; musics count: %s ; \
duration(seconds): %s ; lack(seconds): %s")
          % (str(len(playlist)),
             str(total_duration),
             str(CLI.args.duration - total_duration)))
    # mix final playlist
    playlist = mixlist(playlist)
    log.info(_("mixed final playlist"))

    # output
    output(playlist)


if __name__ == "__main__":
    main()
