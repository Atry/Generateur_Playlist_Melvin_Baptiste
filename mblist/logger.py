"""Logging module"""

import logging
import logging.handlers

from .CLI import args
from .international import init_i18n
_ = init_i18n()


class Log:
    """log class"""
    def __init__(self):
        """Return the log object

        Keyword arguments
        loglevel -- logging level (default logging.ERROR)
        """
        self.loglevel = args.loglevel

        # log formatter (apparence of displayed messages)
        self.formatter_syslog = logging.Formatter('[%(asctime)s] \
%(levelname)s: %(message)s',
                                                  datefmt='%H:%M:%S')
        self.formatter_console = logging.Formatter('%(levelname)s: \
%(message)s')
        # log handlers (how, where go the log)
        # console
        self.logh_console = logging.StreamHandler()
        self.logh_console.setLevel(self.loglevel)
        self.logh_console.setFormatter(self.formatter_console)
        # file
        self.logh_file = logging.handlers.RotatingFileHandler("/tmp/last.log",
                                                              mode='a',
                                                              encoding=None,
                                                              delay=False,
                                                              maxBytes=0,
                                                              backupCount=2)
        self.logh_file.setLevel(logging.DEBUG)
        self.logh_file.setFormatter(self.formatter_syslog)
        self.logh_file.doRollover()
        # syslog
        self.logh_syslog = logging.handlers.SysLogHandler(address='/dev/log',
                                                          facility=logging.handlers.SysLogHandler.LOG_USER
                                                          )
        self.logh_syslog.setLevel(self.loglevel)
        self.logh_syslog.setFormatter(self.formatter_syslog)

        # log object (use with .info(), .warning(), ...)
        self.log = logging.getLogger(__name__)
        self.log.addHandler(self.logh_console)
        self.log.addHandler(self.logh_file)
        self.log.addHandler(self.logh_syslog)
        self.log.setLevel(logging.DEBUG)

        self.log.info(_("created log 'log' in ") + __name__)
        self.log.debug("handlers: " + str(len(self.log.handlers)))

    def debug(self, msg):
        """log debug message"""
        self.log.debug(msg)

    def info(self, msg):
        """log info message"""
        self.log.info(msg)

    def warning(self, msg):
        """log warning message"""
        self.log.warning(msg)

    def error(self, msg):
        """log error message"""
        self.log.error(msg)

    def critical(self, msg):
        """log critical message"""
        self.log.critical(msg)


if __name__ != "__main__":
    log = Log()
