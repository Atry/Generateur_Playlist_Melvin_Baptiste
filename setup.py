from setuptools import setup
setup(
      author="Baptiste Leplat, Melvin Mezerette",
      author_email="baptiste.leplat@bts-malraux.net, melvin.mezerette@bts-malraux.net",
      classifiers=[
          'Development Status :: 2 - Pre_Alpha',
          'Environement :: Console',
          'Intended Audience :: End Users/Desktop',
          'License :: OSI Approved :: GNU General Public Licence v3 or later (GPLv3+)',
          'Natural Language :: English',
          'Operating System :: POSIX :: Linux',
          'Programming Language :: Python :: 3',
          'Topic :: Multimedia :: Sound/Audio',
          'Topic :: Utilities'
      ],
      description="demo setuptools",
      entry_points={
          'console_scripts':[
               'mblist = mblist:main'       
          ]
      },
      install_requires=["psycopg2"],
      keywords="non",
      license="GPLv3+",
      long_description="nonplus",
      data_files=[
                 ('/usr/share/locale/fr/LC_MESSAGES', ['mblist/mblist/fr/LC_MESSAGES/mblist.mo']),
                 ('LICENCE'),
		 ('README'),
                 ('mblist/doc/LaTex/Manuel.pdf'),
                 ('mblist/doc/LaTex/Manuel.html'),
                 ('/usr/share/man/man1/', ['mblist/doc/LaTex/mblist.1.gz'])
                 ],
      name="mblist",
      packages=['mblist'],
      python_requires='>=3.2, <4',
      url="",
      version="1.0.3"
)

