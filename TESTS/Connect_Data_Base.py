# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 14:10:42 2017

@author: melvin
"""
import psycopg2

def main():
    try:
        connexion = psycopg2.connect("dbname='radio_libre' user='m.mezerette' host='172.16.99.2' password='m5k10c30'")
    except Exception as ex:
        print("La connexion à échouer : " + ex.args)
    
    cur = connexion.cursor()
    
    try:
        cur.execute("""SELECT * from morceaux""")
        rows = cur.fetchall()
        for row in rows:
            print ("Donnée musicale = (%s)" %(str(row[0])))        
    except Exception as ex:
        print("La requête à échouer : " + ex.args)
    
    connexion.commit()
    cur.close()
    connexion.close()
        
if __name__ == "__main__":
    main()

