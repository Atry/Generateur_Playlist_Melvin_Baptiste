""" Internationalization module

just import me and write:
    from international import init_i18n
    _ = init_i18n()
"""
import gettext


def init_i18n():
    """return the getext method ('_')"""
    our_domain = 'mblist'
    our_localedir = 'mblist/mblist'
    gettext.bindtextdomain(our_domain, our_localedir)
    gettext.textdomain(our_domain)
    return gettext.gettext
